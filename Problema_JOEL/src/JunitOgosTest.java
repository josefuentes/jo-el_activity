import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class JunitOgosTest {

	@Test
	void testPublic() {
		assertEquals("180", misOgos.calculDioptries("2","15","0"));
		assertEquals("MIS OGOS", misOgos.calculDioptries("1","24","0"));
		assertEquals("180", misOgos.calculDioptries("3","10","0"));
		assertEquals("540", misOgos.calculDioptries("6","15","0"));
		assertEquals("12", misOgos.calculDioptries("2","1","0"));
		assertEquals("0", misOgos.calculDioptries("0","0","0"));
		

	}
	
	@Test
	void testPrivat() {
		assertEquals("MIS OGOS", misOgos.calculDioptries("1","200","0"));
		assertEquals("450", misOgos.calculDioptries("5","15","0"));
		assertEquals("450", misOgos.calculDioptries("3","25","0"));
		assertEquals("396", misOgos.calculDioptries("6","11","0"));
		assertEquals("396", misOgos.calculDioptries("2","33","0"));
		assertEquals("528", misOgos.calculDioptries("4","22","0"));
		assertEquals("MIS OGOS", misOgos.calculDioptries("1","678","0"));
		assertEquals("0", misOgos.calculDioptries("0","11","0"));
		assertEquals("6000000", misOgos.calculDioptries("1000","1000","0"));
	}

}
