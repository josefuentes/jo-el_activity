import java.util.Scanner;

public class misOgos {
	static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args) {
		/**
		 * Al main demanem els casos de prova i inicialitzem un bucle relatiu als
		 * casos de prova. A dins, declarem les variables de c�lcul i les condicions.
		 * Si el num. d' hores es = 1: fem crida a la funci� misOgos()
		 * Si el num. d' hores es qualsevol altre: farem crida a la funci� calculDioptries()
		 * Despr�s, s' imprimeix el resultat
		 * @param casosProva Es el n�mero d' iteracions que es demana a l' usuari
		 * @param i Es el numero d' iteracions del bucle
		 * @param nHores El n�mero d' hores que l' usuari entrar�
		 * @param nAlumnes El n�mero d' alumnes que l' usuari entrar�
		 * @param nDioptries El n�mero de dioptries inicialitzat que despr�s es modificar� amb el valor retornat
		 * 
		 */
		int casosProva = scan.nextInt();
		scan.nextLine();
		
		for(int i = 0; i < casosProva; i++) {
			String nHores = scan.nextLine();
			String nAlumnes = scan.nextLine();
			String nDioptries = "0";
			nDioptries = calculDioptries(nHores, nAlumnes, nDioptries);
			System.out.println(nDioptries);		
		}	
	}
	/**
	 * La funci� calculDioptries s' encarrega de calcular el n�mero de dioptries
	 * @param hores2 les hores que demanem al usuari pasades a Integer
	 * @param alumnes2 el total d' alumnes que demanem al usuari pasats a Integer
	 * @param dioptries2 les dioptries que entren per poder retornar-les amb el calcul fet, es retorna com String
	 * @return Retorna el total de dioptries (hores*((alumnes*2)*3)
	 * 
	 * Si el valor de les hores es igual a 1 (declarat al main), la funci� misOgos tornar� l' String "MIS OGOS"
	 * @param ogos Es l' string que retornar� la funci� si el valor d' hores es igual a 1
	 * @return retornar� l' String "MIS OGOS" 
	 */

	public static String calculDioptries(String hores, String alumnes, String dioptries){
		int hores2 = Integer.parseInt(hores);
		int alumnes2 = Integer.parseInt(alumnes);
		if(hores2 == 1) {
		String misOgos = "MIS OGOS";	
		return misOgos;
		} else {
		String dioptries2 = ""+hores2*((alumnes2*2)*3)+"";
		return dioptries2;
		} 
	}

	

}
